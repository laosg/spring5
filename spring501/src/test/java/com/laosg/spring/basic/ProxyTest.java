package com.laosg.spring.basic;

import com.laosg.spring.proxy.OrderServiceProxy;
import com.laosg.spring.proxy.User;
import com.laosg.spring.proxy.UserService;
import com.laosg.spring.proxy.UserServiceProxy;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/22
 */
public class ProxyTest {

    @Test
    public void test() {
        UserServiceProxy userServiceProxy = new UserServiceProxy();
        userServiceProxy.register(new User());
        userServiceProxy.login("kevi","123");
        System.out.println("------------");
        OrderServiceProxy orderServiceProxy = new OrderServiceProxy();
        orderServiceProxy.showOrder();
    }

    @Test
    public void testAOP() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        //Spring工厂对象通过原始对象的id值获取的是代理对象
        com.laosg.spring.proxy.UserService userService = (UserService) context.getBean("userService");
//        userService.register(new User());
        userService.login("kevin","123");
    }
}
