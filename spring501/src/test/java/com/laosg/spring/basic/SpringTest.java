package com.laosg.spring.basic;


import com.laosg.spring.converter.People;
import com.laosg.spring.factorybean.ConnectionFactoryBean;
import com.laosg.spring.life.Product;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Connection;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
public class SpringTest {

    @Test
    public void test1() {
//        UserService us= new UserServiceImpl();
//        UserService us= BeanFactory.getUserService();
        UserService us= (UserService) BeanFactory.getBean("userService");
        us.login("name","pwd");
        User user = new User();
        user.setName("kevin");
        user.setPassword("123");
        boolean register = us.register(user);
        Assert.assertTrue(register);
    }

    @Test
    public void testSpring() {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//        User user = (User) context.getBean("user");//可以多看看重载的方法
        User user = context.getBean(User.class);
        System.out.println(user.hashCode());
        for (String beanDefinitionName : context.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }

        User auser = (User) context.getBean("auser");
        System.out.println(auser);
        System.out.println(user==auser);
        context.containsBean("user");//可以判断id和name属性
        context.containsBeanDefinition("user");//只能判断id属性
    }


    @Test
    public void testFactoryBean(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//        Connection conn = (Connection) context.getBean("conn");
//        ConnectionFactoryBean connFactoryBean = (ConnectionFactoryBean) context.getBean("&conn");
//        System.out.println(conn);
//        System.out.println(connFactoryBean);
        Product product = (Product) context.getBean("product");
        System.out.println(product.getName());
        People people = (People) context.getBean("people");

        System.out.println(people.getBirthday());



        context.close();
    }



}