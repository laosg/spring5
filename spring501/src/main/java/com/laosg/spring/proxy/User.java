package com.laosg.spring.proxy;

import lombok.Data;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/22
 */
@Data
public class User {
    private String name;
    private int age;
}
