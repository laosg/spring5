package com.laosg.spring.proxy.jdk;

import com.laosg.spring.proxy.User;
import com.laosg.spring.proxy.UserServiceImpl;
import com.laosg.spring.proxy.UserService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * <p>description: some thing </p>
 * 静态代理创建的3要素
 * 原始对象
 * 额外功能
 * 代理对象和原始对象实现相同的接口
 *
 * @author kevinruan@pano.im
 * @date 2020/6/23
 */
public class TestJdkProxy {
    public static void main(String[] args) {
        //创建原始对象
        UserService userService = new UserServiceImpl();
        //JDk创建动态代理(按照方法要求创建)
        //{@link java.lang.reflect.Proxy.newProxyInstance}
        //invocationHandler 作用就是书写额外功能，额外功能运行在方法执行之前/后/抛出异常时
        //proxy 是代表代理对象
        //method 额外功能，所真假个的那个原始方法
        //object[] 原始方法的参数
        UserService us = (UserService) Proxy.newProxyInstance(TestJdkProxy.class.getClassLoader(),
                userService.getClass().getInterfaces(),
                (proxy, method, args1) -> {

                    System.out.println("--log--");
                    //让原始方法执行
                    Object invoke = method.invoke(userService, args1);

                    return invoke;
                });

        us.register(new User());

    }
}
