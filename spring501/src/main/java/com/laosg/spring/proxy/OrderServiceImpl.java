package com.laosg.spring.proxy;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/22
 */
public class OrderServiceImpl implements OrderService {
    @Override
    public void showOrder() {
        System.out.println("com.laosg.spring.proxy.OrderServiceImpl.showOrder");
    }
}
