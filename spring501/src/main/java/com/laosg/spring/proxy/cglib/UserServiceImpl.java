package com.laosg.spring.proxy.cglib;

import com.laosg.spring.proxy.User;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/23
 */
public class UserServiceImpl {
    public void register(User user) {
        System.out.println("UserServiceImpl.register");
    }

    public boolean login(String name, String password) {
        System.out.println("UserServiceImpl.login");
        return true;
    }
}
