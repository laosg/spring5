package com.laosg.spring.proxy;

import com.laosg.spring.dynamic.SysLog;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/22
 */
public class UserServiceImpl implements UserService {
    @Override
    @SysLog
    public void register(User user) {
        System.out.println("UserServiceImpl.register");
    }

    @Override
    public boolean login(String name, String password) {
        System.out.println("UserServiceImpl.login");
        return true;
    }
}
