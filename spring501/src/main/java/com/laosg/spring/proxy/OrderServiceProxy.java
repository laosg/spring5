package com.laosg.spring.proxy;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/22
 */
public class OrderServiceProxy implements OrderService {
    private OrderService orderService=new OrderServiceImpl();
    @Override
    public void showOrder() {
        System.out.println("--log order--");
        orderService.showOrder();
    }
}
