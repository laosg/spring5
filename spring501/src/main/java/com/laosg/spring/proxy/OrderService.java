package com.laosg.spring.proxy;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/22
 */
public interface OrderService {
    void showOrder();
}
