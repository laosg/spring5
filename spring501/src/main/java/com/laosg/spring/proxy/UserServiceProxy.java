package com.laosg.spring.proxy;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/22
 */
public class UserServiceProxy implements UserService {
    private UserService userService=new UserServiceImpl();

    @Override
    public void register(User user) {
        System.out.println("---log start---");//额外的方法
        userService.register(user);
        System.out.println("---log end---");
    }

    @Override
    public boolean login(String name, String password) {
        System.out.println("--log --");
        return userService.login(name,password);
    }
}
