package com.laosg.spring.proxy;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/22
 */
public interface UserService {

    void register(User user);
    boolean login(String name,String password);
}
