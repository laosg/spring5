package com.laosg.spring.basic;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
public class UserServiceImpl implements UserService {

//    UserDao userDao=BeanFactory.getUserDao();
    UserDao userDao= (UserDao) BeanFactory.getBean("userDao");

    public void login(String name, String pwd) {
        userDao.select(name,pwd);
    }

    public boolean register(User user) {
        return userDao.save(user);
    }
}
