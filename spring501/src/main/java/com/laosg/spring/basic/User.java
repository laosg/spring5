package com.laosg.spring.basic;

import lombok.Data;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
@Data
public class User {
    private String name;
    private String password;

    public User(String name, String password) {
        System.out.println("112");
        this.name = name;
        this.password = password;
    }

    public User() {
        System.out.println("not construct");
    }
}
