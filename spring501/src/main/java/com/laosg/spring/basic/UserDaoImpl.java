package com.laosg.spring.basic;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
public class UserDaoImpl implements UserDao {
    public void select(String name, String pwd) {
        System.out.println("get "+name+":"+"from db");
    }

    public boolean save(User user) {
        System.out.println("saved user: "+user.toString());
        return true;
    }
}
