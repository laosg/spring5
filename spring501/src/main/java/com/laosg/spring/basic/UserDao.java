package com.laosg.spring.basic;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
public interface UserDao {
    void select(String name,String pwd);
    boolean save(User user);
}
