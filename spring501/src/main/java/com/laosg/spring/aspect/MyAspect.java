package com.laosg.spring.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * <p>description: some thing </p>
 * 需要额外功能和切入点
 * @author kevinruan@pano.im
 * @date 2020/6/23
 */
@Aspect//表明是切面类
public class MyAspect {


    @Pointcut("execution(* login(..))")
    public void myPointCut(){};
    //额外功能
    @Around("myPointCut()")
    public Object arroud(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("-aspect log-");
        Object ret = joinPoint.proceed();
        return ret;
    }

    @Around("myPointCut()")
    public Object arroud2(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("-aspect2 transaction-");
        Object ret = joinPoint.proceed();
        return ret;
    }
}
