package com.laosg.spring.aspect;

import com.laosg.spring.basic.User;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
public interface UserService {
    void login(String name, String pwd);

    boolean register(User user);
}
