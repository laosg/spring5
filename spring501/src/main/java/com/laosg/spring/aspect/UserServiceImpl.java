package com.laosg.spring.aspect;

import com.laosg.spring.basic.BeanFactory;
import com.laosg.spring.basic.User;
import com.laosg.spring.basic.UserDao;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
public class UserServiceImpl implements UserService {


    @Override
    public void login(String name, String pwd) {
        System.out.println("login");
    }

    @Override
    @Transactional
    public boolean register(User user) {
        System.out.println("register");
        return true;
    }
}
