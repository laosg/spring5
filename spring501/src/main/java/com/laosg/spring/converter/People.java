package com.laosg.spring.converter;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
@Data
public class People {
    private String name;
    private Date birthday;

}
