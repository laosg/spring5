package com.laosg.spring.factorybean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * <p>description: some thing </p>
 * 这个就是实例工厂，主要看配置文件怎么处理
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
public class StaticConnectionFactory {

    public static Connection getConnection() {
        Connection connection=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/laosg", "roor", "123456");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
