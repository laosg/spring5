package com.laosg.spring.factorybean;

import lombok.Data;
import org.springframework.beans.factory.FactoryBean;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
@Data
public class ConnectionFactoryBean implements FactoryBean<Connection> {

    private String className;
    private String url;
    private String name;
    private String password;

    public ConnectionFactoryBean(String className, String url, String name, String password) {
        this.className = className;
        this.url = url;
        this.name = name;
        this.password = password;
    }

    public Connection getObject() throws Exception {
        Class.forName(className);
        Connection connection = DriverManager.getConnection(url, name, password);
        return connection;
    }

    public Class<?> getObjectType() {
        return Connection.class;
    }
}
