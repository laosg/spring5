package com.laosg.spring.life;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/21
 */
public class Product implements InitializingBean, DisposableBean {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("set name");
        this.name = name;
    }

    public Product() {
        System.out.println("com.laosg.spring.life.Product");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("完成初始化操作");
    }

    public void init() {
        System.out.println("init function");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("完成销毁动作");
    }
    public void end() {
        System.out.println("end");
    }


}
