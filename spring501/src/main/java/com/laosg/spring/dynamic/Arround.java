package com.laosg.spring.dynamic;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * <p>description: some thing </p>
 *
 * @author kevinruan@pano.im
 * @date 2020/6/22
 */
public class Arround implements MethodInterceptor {
    /**
     * 可以运行在原属方法之前/之后
     * @param invocation
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        System.out.println("之前");
        Object proceed = null;
        try {
            proceed = invocation.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        System.out.println("之后");
        return proceed;
    }
}
